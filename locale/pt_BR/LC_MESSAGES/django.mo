��          �      \      �     �     �     �               #  0   )  
   Z  	   e     o     t  	   }  >   �     �     �     �     �     �       A       J     [     j     {     �     �  3   �     �     �     �     �       B        T     `     m     �     �  	   �                	                  
                                                                 Account Number Checking Account Checking Accounts Created Date Creation datetime. Email Insufficient funds to complete this transaction. Is Active? Is Staff? Name Password Public ID This account number does not match an active Checking Account. Transaction Transactions Update datetime. Updated Date User Users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Número da Conta Conta Corrente Contas Correntes Data de Criação Data de criação E-mail Saldo insuficiente para completar esta transação. Está Ativo? Faz parte do Time? Nome Senha ID Público Este número de conta não corresponde a uma Conta Corrente ativa. Transação Transações Data de Atualização Data de Atualização Usuário Usuários 