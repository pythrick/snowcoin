import pytest
from faker import Faker
from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import APIRequestFactory

from app.views import WalletViewSet


@pytest.fixture
def factory():
    return APIRequestFactory()


@pytest.fixture
def fake():
    return Faker()


@pytest.mark.django_db
class TestWalletViewSet:
    def test_wallet_view_set(self, factory):
        wallet = mixer.blend('app.Wallet')
        view = WalletViewSet.as_view({'get': 'retrieve'})
        request = factory.get('/wallets/{}'.format(wallet.account_number))
        response = view(request, account_number=wallet.account_number)
        assert response.status_code == status.HTTP_200_OK
        assert response.data['account_number'] == wallet.account_number
        assert response.data['balance'] == wallet.balance
