from dataclasses import dataclass


@dataclass
class BaseError(Exception):
    message: str

    def __str__(self):
        return str(self.message)
