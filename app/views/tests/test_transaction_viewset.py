import pytest
from faker import Faker
from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate

from app.exceptions import WalletNotFoundError
from app.views.transaction import TransactionViewSet


@pytest.fixture
def factory():
    return APIRequestFactory()


@pytest.fixture
def fake():
    return Faker()


@pytest.mark.django_db
class TestTransactionViewSet:
    def test_wallet_view_set(self, factory):
        wallet = mixer.blend('app.Wallet')
        transaction = mixer.blend('app.Transaction', wallet=wallet, amount=100)

        view = TransactionViewSet.as_view({'post': 'create'})

        request = factory.post('/transactions/',
                               {
                                   "wallet": wallet.account_number,
                                   "amount": transaction.amount
                               })
        user = mixer.blend('app.User')
        force_authenticate(request, user=user)
        response = view(request)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['wallet'] == wallet.account_number
        assert response.data['amount'] == transaction.amount
        assert response.data['balance'] == wallet.balance

    def test_wallet_view_set_with_error(self, factory, fake):
        view = TransactionViewSet.as_view({'post': 'create'})

        request = factory.post('/transactions/',
                               {
                                   "wallet": fake.pyint(),
                                   "amount": fake.pydecimal(left_digits=5, right_digits=2, positive=True)
                               })
        user = mixer.blend('app.User')
        force_authenticate(request, user=user)
        response = view(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['message'] == WalletNotFoundError.message
