# Generated by Django 2.2.3 on 2019-07-08 21:30

import app.models.checking_account
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='Public ID')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Email')),
                ('password', models.CharField(max_length=128, verbose_name='Password')),
                ('name', models.CharField(max_length=150, verbose_name='Name')),
                ('is_active', models.BooleanField(default=True, verbose_name='Is Active?')),
                ('is_staff', models.BooleanField(default=False, verbose_name='Is Staff?')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='Creation datetime.', verbose_name='Created Date')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='Update datetime.', null=True, verbose_name='Updated Date')),
            ],
            options={
                'verbose_name': 'User',
                'verbose_name_plural': 'Users',
                'db_table': 'users',
                'ordering': ('created_at',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CheckingAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account_number', models.PositiveSmallIntegerField(default=app.models.checking_account.generate_account_number, editable=False, unique=True, verbose_name='Account Number')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='Creation datetime.', verbose_name='Created Date')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='Update datetime.', null=True, verbose_name='Updated Date')),
            ],
            options={
                'verbose_name': 'Checking Account',
                'verbose_name_plural': 'Checking Accounts',
                'db_table': 'checking_accounts',
                'ordering': ('created_at',),
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='Public ID')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='Value')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='Creation datetime.', verbose_name='Created Date')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='Update datetime.', null=True, verbose_name='Updated Date')),
                ('authorized_by', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='authorized_transactions', to=settings.AUTH_USER_MODEL)),
                ('checking_account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='transactions', to='app.CheckingAccount')),
            ],
            options={
                'verbose_name': 'Transaction',
                'verbose_name_plural': 'Transactions',
                'db_table': 'transactions',
                'ordering': ('created_at',),
            },
        ),
    ]
