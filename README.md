[![pipeline status](https://gitlab.com/pwall/snowcoin/badges/master/pipeline.svg)](https://gitlab.com/pwall/snowcoin/commits/master)
[![coverage report](https://gitlab.com/pwall/snowcoin/badges/master/coverage.svg)](https://gitlab.com/pwall/snowcoin/commits/master)
 

# Snowcoin Webservice

Snow Coin é moeda digital para transações da pagamentos do evento da Festa Julina 
da Snowman Labs que visa substituir a utilização da moedas de papel.

## Links
- Web Client para Consultar Saldo: http://snowcoin.snowmanlabs.com.s3-website-us-east-1.amazonaws.com
- Web Client para Registrar Transações: http://snowcoin-admin.snowmanlabs.com.s3-website-us-east-1.amazonaws.com
- API: https://snowcoin-webservice.herokuapp.com/api/v1/

## Justificativa

Evitar a necessidade de:
- fazer uma estimativa imprecisa da quantidade de dinheiro fake a ser impresso
- imprimir e cortar diversas notas que serão descartadas no final da festa
- ter troco nas barraquinhas
- distribuir notas pequenas para saldo inicial
- os usuários baixar aplicativo

Evitar a possibilidade de:
- empréstimo e doação de moedas entre os participantes
- obtenção de moedas por meio de atividades "não autorizadas"

Diferenciais:
- Utilizar uma solução digital que faça parte do contexto da empresa (apps/makers)


## Proposta

- Cada participante recebe um número referente a uma conta corrente digital.
- Todas as contas correntes começam com um saldo inicial. Por exemplo 250,00
Snow Coins.
- Para participar das brincadeiras nas barraquinhas da festa será necessário
gastar Snow Coins. Dependendo do desempenho dos participantes, estes poderão
ganhar novas Snow Coins.
- Os atendendentes de barraquinhas terão acesso à um App (ou página WEB) 
com acesso restrito para registrar as transações (DÉBITO e DEPÓSITO) dos
participantes.
- Ao chegar na barraquinha o participante informa o seu número de conta 
corrente, o atendente coloca no App o número recebido, o valor de Snow Coins 
à serem debitados, tipo de transação "DÉBITO" e aperta no botão "Fazer 
transação".  
- Ao final da brincadeira, se o participante atingir pontuação para ganhar
novos Snow Coins, o atendente coloca no APP o código do participante,
o valor de Snow Coins a serem depositados na conta do participante, tipo de 
transação "DEPÓSITO" e aperta no botão "Fazer transação".
- O serviço sempre retorna se cada transação ocorreu com sucesso ou não, 
assim como o saldo do atual de Snow Coins do participante.
- O participante pode consultar o saldo de sua conta corrente apenas 
informando o número de sua conta em uma página web. 
- A URL página de consulta de saldo ficará disponível juntamente com seu
QR code impressos em folhas A4 fixadas nas paredes do local da festa.
- Nos horários pré-fixados para resgate de brindes os participantes deverão 
informar o número de sua conta corrente na barraquinha de trocas de brindes, 
o atendente informa no App o número recebido, o valor em Snow Coins referente 
ao brinde a ser resgatado, tipo de transação "DÉBITO" e aperta em "Fazer 
transação".

#### Ideias adicionais
- Para entregar os códigos de contas-correntes, entregar pulseirinhas de 
eventos contendo o número de conta corrente + QR code (talvez)
https://www.isofestascuritiba.com.br/produto/pulseira-de-identificacao-fluor-c-50-un/
- Totem para consulta de saldo de conta corrente de quem estiver sem celular
e/ou internet.
- Gerador de QR Code: https://br.qr-code-generator.com/


## Recursos necessários

- [x] API com 2 endpoints (consultar saldo e fazer transação)
- [x] Página web para consultar o saldo
- [x] Página web com autenticação para registrar transações
- [x] Gerar o QR code
- [ ] Imprimir o QR em 2-4 folhas para fixar nas paredes com a URL de consulta de saldo
- [ ] Gerar contas dos administradores
- [ ] Comprar folhas de etiquetas para impressão
- [ ] Imprimir os números das contas correntes em etiquetas


## Exemplos de requisições na API

GET /api/v1/checking-accounts/1435/

Response: 200 OK
```json
{
  "balance": 170.0,
  "last_transaction": "2019-06-10T18:35:23"
}
```


@authenticated <br>
POST /api/v1/transactions/

```json
{
  "account_number": 1435,
  "amount": 20.0
}
```


Response: 201 OK
```json
{
  
  "id": "6edf8972-01fd-4bce-ad17-7edf3648825a",
  "account_number": 1435,
  "amount": 20.0,
  "balance": 150.0
}
```
