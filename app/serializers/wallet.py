from rest_framework import serializers

from app.models import Wallet


class WalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ('account_number', 'balance')
