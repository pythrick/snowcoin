from rest_framework import viewsets, permissions

from app.models import Transaction
from app.serializers import TransactionSerializer


class TransactionViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing checking accounts.
    """
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = 'uuid'
