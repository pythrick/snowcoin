from rest_framework import viewsets, permissions

from app.models import Wallet
from app.serializers import WalletSerializer


class WalletViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing checking accounts.
    """
    queryset = Wallet.objects.all()
    serializer_class = WalletSerializer
    permission_classes = (permissions.AllowAny,)
    lookup_field = 'account_number'
