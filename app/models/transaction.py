import uuid

from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _

from app.exceptions.transaction import InsufficientFundsError


class Transaction(models.Model):
    class Meta:
        app_label = 'app'
        db_table = 'transactions'
        ordering = ('created_at',)
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')

    uuid = models.UUIDField(
        verbose_name=_('Public ID'),
        default=uuid.uuid4,
        editable=False,
        unique=True)

    wallet = models.ForeignKey(
        to="Wallet",
        on_delete=models.CASCADE,
        related_name='transactions',
        null=False)

    amount = models.DecimalField(
        verbose_name="Value",
        max_digits=7,
        decimal_places=2,
        null=False)

    authorized_by = models.ForeignKey(
        to="User",
        on_delete=models.PROTECT,
        related_name='authorized_transactions',
        null=False
    )

    created_at = models.DateTimeField(
        verbose_name=_('Created Date'),
        auto_now_add=True,
        null=False,
        blank=False,
        help_text=_('Creation datetime.'))

    updated_at = models.DateTimeField(
        verbose_name=_('Updated Date'),
        auto_now=True,
        null=True,
        blank=False,
        help_text=_('Update datetime.'))

    def __str__(self):
        return '{}: {}'.format(self.wallet, self.amount)

    def __repr__(self):
        return 'Transaction<{}: {}>'.format(self.wallet, self.amount)

    @transaction.atomic
    def save(self, *args, **kwargs):
        created = self.pk is None
        super(Transaction, self).save(*args, **kwargs)
        self.validate_after_saving(created)

    def validate_after_saving(self, created: bool):
        if self.wallet.balance < 0:
            raise InsufficientFundsError
