import traceback
from . import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code
    else:
        if settings.DEBUG:
            traceback.print_tb(exc.__traceback__)
        response = Response({'message': str(exc)}, status.HTTP_400_BAD_REQUEST)

    return response
