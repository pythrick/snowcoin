import pytest
from faker import Faker
from mixer.backend.django import mixer

from app.exceptions import WalletNotFoundError
from app.serializers import TransactionSerializer


@pytest.fixture
def fake():
    return Faker()


@pytest.fixture
def user():
    return mixer.blend('app.User')


@pytest.mark.django_db
class TestTransactionSerializer:
    def test_serialize_transaction(self, fake, user):
        wallet = mixer.blend('app.Wallet')
        transaction = mixer.blend('app.Transaction',
                                  wallet=wallet,
                                  amount=fake.pydecimal(left_digits=5, right_digits=2, positive=True),
                                  authorized_by=user)
        serializer = TransactionSerializer(transaction)
        assert serializer.data
        assert serializer.data['wallet'] == transaction.wallet.account_number
        assert serializer.data['amount'] == transaction.amount
        assert serializer.data['created_at'] == transaction.created_at.isoformat()

    def test_serialize_transaction_with_non_existent_wallet(self, fake):
        data = {
            'wallet': fake.pyint(),
            'amount': fake.pydecimal(left_digits=5, right_digits=2, positive=True)
        }
        serializer = TransactionSerializer(data=data)
        with pytest.raises(WalletNotFoundError) as e:
            assert serializer.is_valid(raise_exception=True)
        assert e.value.message == WalletNotFoundError.message
