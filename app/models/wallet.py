import random

from django.db import models
from django.utils.translation import ugettext_lazy as _


def generate_account_number():
    account_number = random.randint(3000, 9999)
    if not Wallet.objects.filter(account_number=account_number).exists():
        return account_number
    return generate_account_number()


class Wallet(models.Model):
    class Meta:
        app_label = 'app'
        db_table = 'wallets'
        ordering = ('created_at',)
        verbose_name = _('Checking Account')
        verbose_name_plural = _('Checking Accounts')

    account_number = models.PositiveSmallIntegerField(
        verbose_name=_('Account Number'),
        unique=True,
        default=generate_account_number,
        editable=False,
        null=False)

    created_at = models.DateTimeField(
        verbose_name=_('Created Date'),
        auto_now_add=True,
        null=False,
        blank=False,
        help_text=_('Creation datetime.'))

    updated_at = models.DateTimeField(
        verbose_name=_('Updated Date'),
        auto_now=True,
        null=True,
        blank=False,
        help_text=_('Update datetime.'))

    def __str__(self):
        return str(self.account_number)

    def __repr__(self):
        return 'Wallet<{}>'.format(self.account_number)

    @property
    def balance(self):
        return sum(t.amount for t in self.transactions.all())
