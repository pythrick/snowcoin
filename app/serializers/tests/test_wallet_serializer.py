import pytest
from faker import Faker
from mixer.backend.django import mixer

from app.serializers import WalletSerializer


@pytest.fixture
def fake():
    return Faker()


@pytest.fixture
def user():
    return mixer.blend('app.User')


@pytest.mark.django_db
class TestWalletSerializer:
    def test_serialize_wallet(self, fake, user):
        wallet = mixer.blend('app.Wallet')
        mixer.cycle(5).blend('app.Transaction',
                             wallet=wallet,
                             amount=fake.pydecimal(left_digits=5, right_digits=2, positive=True),
                             authorized_by=user)
        serializer = WalletSerializer(wallet)
        assert serializer.data
        assert serializer.data['account_number'] == wallet.account_number
        assert serializer.data['balance'] == wallet.balance
