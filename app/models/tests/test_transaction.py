import pytest
from faker import Faker

from mixer.backend.django import mixer

from app.exceptions import InsufficientFundsError


@pytest.fixture
def fake():
    return Faker()


@pytest.fixture
def user():
    return mixer.blend('app.User')


@pytest.mark.django_db
class TestTransaction:

    def test_create_instance(self, fake, user):
        obj = mixer.blend(
            'app.Transaction',
            amount=fake.pydecimal(left_digits=5, right_digits=2, positive=True),
            authorized_by=user
        )
        assert obj.pk is not None, 'Should create a Transaction instance'

    def test_string(self, fake, user):
        obj = mixer.blend(
            'app.Transaction',
            amount=fake.pydecimal(left_digits=5, right_digits=2, positive=True),
            authorized_by=user
        )
        assert str(obj) == '{}: {}'.format(obj.wallet, obj.amount), ('Transaction string should be its '
                                                                               'checking account with the value.')

    def test_representation(self, fake, user):
        obj = mixer.blend(
            'app.Transaction',
            amount=fake.pydecimal(left_digits=5, right_digits=2, positive=True),
            authorized_by=user
        )
        assert repr(obj) == 'Transaction<{}: {}>'.format(obj.wallet, obj.amount), ('Transaction '
                                                                                             'representation should '
                                                                                             'be its class name with '
                                                                                             'checking account and '
                                                                                             'the value.')

    def test_deposit_value_to_account(self, fake, user):
        wallet = mixer.blend('app.Wallet')
        initial_balance = wallet.balance
        transaction = mixer.blend(
            'app.Transaction',
            wallet=wallet,
            amount=fake.pydecimal(left_digits=5, right_digits=2, positive=True),
            authorized_by=user
        )
        assert wallet.balance == (initial_balance + transaction.amount), ('The new checking account balance '
                                                                                    'should be sum of initial balance '
                                                                                    'and the transaction value.')

    def test_debit_value_from_account(self, fake, user):
        wallet = mixer.blend('app.Wallet')
        mixer.blend(
            'app.Transaction',
            wallet=wallet,
            amount=100,
            authorized_by=user
        )
        mixer.blend(
            'app.Transaction',
            wallet=wallet,
            amount=-50.0,
            authorized_by=user
        )
        assert wallet.balance == 50.0, ('The new checking account balance should be sub of initial balance '
                                                  'and the transaction value.')

    def test_debit_value_bigger_then_account_balance(self, fake, user):
        wallet = mixer.blend('app.Wallet')
        initial_balance = wallet.balance
        with pytest.raises(InsufficientFundsError) as e:
            assert mixer.blend(
                'app.Transaction',
                wallet=wallet,
                amount=-100.00,
                authorized_by=user
            )
        assert e.value.message == InsufficientFundsError.message
        assert initial_balance == wallet.balance, ('The checking account balance should have been kept'
                                                             'unchanged.')

    def test_update_transaction_in_order_to_get_negative_account_balance(self, fake, user):
        wallet = mixer.blend('app.Wallet')
        transaction = mixer.blend(
            'app.Transaction',
            wallet=wallet,
            amount=100.00,
            authorized_by=user
        )
        with pytest.raises(InsufficientFundsError) as e:
            transaction.amount = -300
            assert transaction.save()
        assert e.value.message == InsufficientFundsError.message
