from rest_framework import serializers
from rest_framework.fields import empty

from app.exceptions import WalletNotFoundError
from app.models import Transaction, Wallet


class TransactionSerializer(serializers.ModelSerializer):
    wallet = serializers.SlugRelatedField(slug_field='account_number', queryset=Wallet.objects.all())
    balance = serializers.ReadOnlyField(source='wallet.balance', read_only=True)
    authorized_by = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = Transaction
        fields = ('uuid', 'wallet', 'amount', 'balance', 'authorized_by', 'created_at')

    def run_validation(self, data: dict = empty):
        wallet = data.get('wallet')
        if wallet:
            self.check_if_wallet_exists(wallet)
        return super(TransactionSerializer, self).run_validation(data)

    @staticmethod
    def check_if_wallet_exists(account_number: str):
        if not Wallet.objects.filter(account_number=account_number).exists():
            raise WalletNotFoundError
