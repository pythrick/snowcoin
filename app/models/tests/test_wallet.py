import pytest
from faker import Faker

from mixer.backend.django import mixer

from app.exceptions import InsufficientFundsError


@pytest.fixture
def fake():
    return Faker()


@pytest.fixture
def user():
    return mixer.blend('app.User')


@pytest.mark.django_db
class TestWallet:

    def test_create_instance(self):
        obj = mixer.blend('app.Wallet')
        assert obj.pk is not None, 'Should create a Wallet instance'

    def test_string(self):
        obj = mixer.blend('app.Wallet')
        assert str(obj) == str(obj.account_number), 'Wallet string should be its account number'

    def test_representation(self):
        obj = mixer.blend('app.Wallet')
        assert repr(obj) == 'Wallet<{}>'.format(obj.account_number), ('Wallet representation should '
                                                                               'be its class name with account number')

    def test_balance(self, fake, user):
        wallet = mixer.blend('app.Wallet')
        transactions = mixer.cycle(5).blend('app.Transaction',
                                            wallet=wallet,
                                            amount=fake.pydecimal(left_digits=5, right_digits=2, positive=True),
                                            authorized_by=user)
        assert wallet.balance == sum(t.amount for t in transactions)

    def test_generate_repeated_account_number(self, mocker):
        mixer.blend('app.Wallet', account_number=1111)
        mocker.patch('random.randint', return_value=1111)
        with pytest.raises(RecursionError) as e:
            assert mixer.blend('app.Wallet')
