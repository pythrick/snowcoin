from dataclasses import dataclass

from django.utils.translation import ugettext_lazy as _

from app.exceptions import BaseError


@dataclass
class WalletNotFoundError(BaseError):
    message: str = _("This account number does not match an active Checking Account.")
