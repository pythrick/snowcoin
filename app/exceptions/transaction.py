from dataclasses import dataclass

from django.utils.translation import ugettext_lazy as _

from app.exceptions import BaseError


@dataclass
class InsufficientFundsError(BaseError):
    message: str = _('Insufficient funds to complete this transaction.')
